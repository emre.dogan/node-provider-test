const functions = {
  hello: name => console.log(`Hello ${name}`),
  sayMyName: name => console.log(`Your name is ${name}`),
  new: name => console.log(`your new name is ${name.toUpperCase()}`)
};

module.exports = functions;
